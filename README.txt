CONTENTS OF THIS FILE
---------------------

 * Author
 * Description
 * Installation
 * Upgrading from Portable Path
 * Configuration
 * Usage

AUTHOR
------
Original Author:
Jim Berry ("solotandem", http://drupal.org/user/240748)

Fork Author:
Guy Elsmore-Paddock ("GuyPaddock", https://www.drupal.org/u/guypaddock)

DESCRIPTION
-----------
This project does two things with respect to file and image URLs entered in text
fields (usually long text with or without summary):

  - when editing content, it saves the URLs in a portable fashion using either
    stream wrapper notation or file ID tokens
  - when rendering the text field, it converts the portable path to either an
    absolute or relative URL

The motivation behind this is to store URLs in text fields (e.g. node body) in a
portable fashion so that a database can be ported across environments and
domains. For example, if the environment consists of development, stage, and
production URLs like:

  - foo-dev.example.com
  - foo-stage.example.com
  - foo.example.com

with files stored in the public files directory which may be named the same as
the domain like:

  - sites/foo-dev.example.com/image
  - sites/foo-stage.example.com/image
  - sites/foo.example.com/image

This would be a typical setup using a provisioning system like Aegir which,
during site creation, allows for specification of the domain name but not the
directory name.

This module will also enable the content to be portable during a domain name
change such as from example.com to rename.com.

INSTALLATION
------------
To use this module, install it in a modules directory on the local site. See
http://drupal.org/node/895232 for further information.

UPGRADING FROM PORTABLE PATH
----------------------------
This module is intended as a drop-in replacement for Portable Path. You will
need to disable and uninstall Portable Path before installing Portable Plus.

Once Portable Path Plus is installed, any modules that previously depended on
Portable Path (`portable_path`) should automatically recognize Portable Path
Plus (`portable_path_plus`) as satisfying their dependency.

If you cannot disable and uninstall Portable Path because of other modules that
depend on it, install and enable Portable Path Plus (ignore the warning you
receive), then you should be able to disable and uninstall Portable Path.

CONFIGURATION
-------------
Visit the module configuration page at admin/config/content/portable to indicate
the fields and stream wrappers to be converted. The portable storage format to
use is specified on a text format configuration page, for example, at
admin/config/content/formats/filtered_html.

USAGE
-----
These filters can be easily used with the Insert module which inserts an
absolute or relative URL. On save of the content, this module will replace the
URL with a path using stream wrapper notation or a file ID token.
