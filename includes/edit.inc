<?php
/**
 * @file
 * Provides filtering of encoded URLs in fields being edited based on settings.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_field_widget_form_alter().
 *
 * This hook is called on entity edit. By default, the unfiltered text (e.g.,
 * from the value or summary column) is displayed in the textarea field. In
 * order for a user to edit this text or for an image to display in an WYSIWYG
 * editor, the portable path filters need to be applied to the raw text value.
 *
 * Unlike the media module which does its business using JavaScript (applying
 * its filter logic to render the WYSIWYG output, but displaying the raw token
 * element in the source view), our filters can simply be applied to the raw
 * input. The file source will be converted to a portable token on entity save.
 *
 * If this module is used with remote_content, this approach allows filters to
 * be applied consistently on the local site during entity view instead of on
 * the remote site during entity load. To enable this clean separation of the
 * content bits from the render bits, one of the text filters will need to have
 * its 'cache' property set to zero to disable the caching of the filtered text.
 * This separation is important with filters like media_wysiwyg and picture that
 * go beyond simple text filtering and apply rendering changes.
 */
function _portable_path_plus_field_widget_form_alter(&$element, &$form_state,
                                                     $context) {
  $delta = $context['delta'];
  if (empty($context['items']) || empty($context['items'][$delta])) {
    // Field is not set, i.e., no value.
    return;
  }

  $settings = portable_path_plus_config();

  if (!in_array($context['instance']['field_name'], $settings['field_names'])) {
    // Filters are not to be applied to this field.
    return;
  }

  // Extract information.
  $items = $context['items'];

  if (!isset($items[$delta]['format'])) {
    // Field not configured with text formats in this bundle.
    return;
  }

  $format = $items[$delta]['format'];

  // The first column does not have an extra level in the element array.
  $column = current($element['#columns']);

  // Gather information.
  $formats = filter_formats();
  $filter_info = portable_path_plus_filter_info();

  $filters = filter_list_format($format);
  // Which of our filters is active in the format and in what order?
  $our_filters = array_filter($filters, 'portable_path_plus_array_filter');

  if (empty($our_filters)) {
    return;
  }

  // @todo At this point we could call:
  //   $function = $instance['widget']['module'] . '_field_widget_form';
  // to regenerate the form element and let it deal with array structures and
  // naming conventions.
  // OR, we can assume all fields are like text fields with or without summary
  // and just set the #default_value.

  // Apply filters to non-format columns.
  $extra_columns = array_diff($element['#columns'], array('format'));
  foreach ($extra_columns as $extra_column) {
    foreach (array_keys($our_filters) as $key) {
      $function = $filter_info[$key]['process callback'];
      if (function_exists($function)) {
        $filter = $our_filters[$key];
        if ($extra_column == $column) {
          $default_value = &$element['#default_value'];
        }
        else {
          $default_value = &$element[$extra_column]['#default_value'];
        }
        if ($default_value) {
          $default_value =
            $function($default_value, $filter, $formats[$format]);
        }
      }
    }
  }
}
